sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		createJSONModel: function() {
			var json = {
				BusinessPartnerSet: [
					{BpId:"100000000",CompanyName:"SAP",Country:"DE",City:"Walldorf",
					PhoneNumber:"622734567",BpRole:"01",WebAddress:"http://www.sap.com",
					EmailAddress:"do.not.reply@sap.com",PostalCode:"59190",Street:"Dietmar-Hopp-Allee",
					Products:[
						{ProductId:"HT-1000",Category:"Notebooks",Name:"Notebook Basic 15",Description:"Notebook Basic 15 with 2,80 GHz quad core"},
						{ProductId:"HT-1092",Category:"Speakers",Name:"Sound Booster",Description:"PC multimedia speaker"},
						{ProductId:"HT-2026",Category:"Computer System Accessories",Name:"Audio/Video Cable Kit",Description:"Quality cables for notebooks"}
					]},
					{BpId:"100000001",CompanyName:"Becker Berlin",Country:"DE",City:"Berlin",
					PhoneNumber:"3088530",BpRole:"01",WebAddress:"http://www.beckerberlin.de",
					EmailAddress:"dagmar.schulze@beckerberlin.de",PostalCode:"59190",Street:"Calvinstra",
					Products:[
						{ProductId:"HT-1001",Category:"Notebooks",Name:"Notebook Basic 17",Description:"Notebook Basic 17 with 2,80 GHz quad core"},
						{ProductId:"HT-1100",Category:"Software",Name:"Smart Office",Description:"Complete package"},
						{ProductId:"HT-2025",Category:"Computer System Accessories",Name:"CD/DVD case",Description:"CD/DVD case"}
					]}
				]	
			};
			var oModel = new JSONModel(json);
			return oModel;
		}

	};
});